----- MOTIVATION -----
We are moving in on a ship and under severe tension both ships fired warnings at each other. We however will engage in battle 

-----------  RULES of ENGAGEMENT  -----------
- all messages are sent in caps
- only allowed communication is [A-Z]+[.] characters
- All communications end with a dot . 
- no shorthands, all spaces, spellings are kept intact



---------------   ORIGINAL   ---------------
THE SHIP IS UNDER FIRE.

WHAT DO YOU SUGGEST.

WE SHOULD TAKE COVER.

THE CAPTAIN WILL DECIDE.

THE CAPTAIN IS NOT HERE.

WE CAN WAIT FOR HIM.

THERE IS NO TIME FOR THAT.

WE ARE ON OUR OWN.

WE SENT AN ORDER.

OVER.

ATTACK AT DAWN.



---------------   H  E  X   -----------------

54 48 45 20 53 48 49 50 20 49 53 20 55 4e 44 45 52 20 46 49 52 45 2e

57 48 41 54 20 44 4f 20 59 4f 55 20 53 55 47 47 45 53 54 2e

57 45 20 53 48 4f 55 4c 44 20 54 41 4b 45 20 43 4f 56 45 52 2e

54 48 45 20 43 41 50 54 41 49 4e 20 57 49 4c 4c 20 44 45 43 49 44 45 2e

54 48 45 20 43 41 50 54 41 49 4e 20 49 53 20 4e 4f 54 20 48 45 52 45 2e

57 45 20 43 41 4e 20 57 41 49 54 20 46 4f 52 20 48 49 4d 2e

54 48 45 52 45 20 49 53 20 4e 4f 20 54 49 4d 45 20 46 4f 52 20 54 48 41 54 2e

57 45 20 41 52 45 20 4f 4e 20 4f 55 52 20 4f 57 4e 2e

57 45 20 53 45 4e 54 20 41 4e 20 4f 52 44 45 52 2e

4f 56 45 52 2e

41 54 54 41 43 4b 20 41 54 20 44 41 57 4e 2e

			        |
				|		
key = 
98 fc 71 86 28 95 21 49 6a ce 2e f2 16 d1 56 19 92 a3 40 c9 34 cd f0 d1 e7 1e 


---------------   CIPHER TEXT  ---------------


CC B4 34 A6 7B DD 68 19 4A 87 7D D2 43 9F 12 5C C0 83 06 80 66 88 DE

CF B4 30 D2 08 D1 6E 69 33 81 7B D2 45 84 11 5E D7 F0 14 E7
 
CF B9 51 D5 60 DA 74 05 2E EE 7A B3 5D 94 76 5A DD F5 05 9B 1A 

CC B4 34 A6 6B D4 71 1D 2B 87 60 D2 41 98 1A 55 B2 E7 05 8A 7D 89 B5 FF 

CC B4 34 A6 6B D4 71 1D 2B 87 60 D2 5F 82 76 57 DD F7 60 81 71 9F B5 FF 

CF B9 51 C5 69 DB 01 1E 2B 87 7A D2 50 9E 04 39 DA EA 0D E7

CC B4 34 D4 6D B5 68 1A 4A 80 61 D2 42 98 1B 5C B2 E5 0F 9B 14 99 B8 90 B3 30

CF B9 51 C7 7A D0 01 06 24 EE 61 A7 44 F1 19 4E DC 8D 

CF B9 51 D5 6D DB 75 69 2B 80 0E BD 44 95 13 4B BC 

D7 AA 34 D4 06 

----------------
|Challenge CT: |
----------------
D9 A8 25 C7 6B DE 01 08 3E EE 6A B3 41 9F 78 